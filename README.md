# POC Logs analysis with ELK (Elasticsearch-Logstash-Kibana) Stack

## Steps to deploy the POC

1. Build the docker image personalized of logstash:
```
cd logstash_image && docker build -t logstash_nginx:7.10.0 .
```
2. Launch the ELK stack
```bash
cd .. && docker-compose up -d 
```

`
N.B.: ⚠️⚠️⚠️⚠️⚠️⚠️ You may have to adpat the grok of the conf of logstash to match the date format or some other infos according to the sample of logs you have (e.g. the date format might vary from a server config to another !!!) ⚠️⚠️⚠️⚠️
`

3. Wait about 2 minutes for the stack to be deployed till end.
```bash
sleep 120
```

4. check deployment of elasticsearch and kibana
```bash
curl localhost:9200
```
```bash
curl localhost:5601
```

5. Check the list of index created on elascticsearch and check the presence of index starting with `nginx-logs-`
```bash
curl -X GET "http://localhost:9200/_cat/indices?v"
```

6. Look content of the index `nginx-logs-yyyy-mm-dd`
```bash
curl -X GET "http://localhost:9200/nginx-logs-yyyy-mm-dd/_search"
```

7. Go to Kibana interface (`http:<IP_Server>:5601`)
* Create a pattern of index to monitor (stack management -> index pattern), then enter the prefix of the index to manage (`nginx-logs*`) next select `@timestamp` as the time field and then create the index pattern.

* Go the Discover from the home page left panel, and there see the panel of the logs monitoring

8. Enjoy : you can now filter and analyse logs as you wish





